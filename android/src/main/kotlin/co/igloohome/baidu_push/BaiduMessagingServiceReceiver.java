package co.igloohome.baidu_push;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.baidu.android.pushservice.PushMessageReceiver;
import java.util.List;


/*
 * 10001 - Network Problem
 * 10101 - Integrate Check Error
 * 30600 - Internal Server Error
 * 30601 - Method Not Allowed
 * 30602 - Request Params Not Valid
 * 30603 - Authentication Failed
 * 30604 - Quota Use Up Payment Required
 * 30605 - Data Required Not Found
 * 30606 - Request Time Expires Timeout
 * 30607 - Channel Token Timeout
 * 30608 - Bind Relation Not Found
 * 30609 - Bind Number Too Many
 */

public class BaiduMessagingServiceReceiver extends PushMessageReceiver {
    String ACTION_REMOTE_MESSAGE = "baidu.push.message";
    String ACTION_NOTIFICATION_CLICKED = "baidu.push.notif.clicked";
    String ACTION_REMOTE_MESSAGE_BIND = "baidu.push.bind";
    String ACTION_REMOTE_MESSAGE_UNBIND = "baidu.push.unbind";
    private String MESSAGE_TAG = "message_description";
    private String MESSAGE_TITLE_TAG = "message_title";
    private String CUSTOM_MESSAGE_TAG = "message_content";
    public static final String TAG = BaiduMessagingServiceReceiver.class
            .getSimpleName();

    @Override
    public void onBind(Context context, int errorCode, String appid,
                       String userId, String channelId, String requestId) {
        String responseString = "onBind errorCode=" + errorCode + " appid="
                + appid + " userId=" + userId + " channelId=" + channelId
                + " requestId=" + requestId;
        Log.d(TAG, "onbind call");
        Log.d(TAG, responseString);
        Intent intent = new Intent(ACTION_REMOTE_MESSAGE_BIND);
        String TOKEN_TAG = "token";
        intent.putExtra(TOKEN_TAG, userId);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
    @Override
    public void onMessage(Context context, String message,
                          String customContentString) {
        String messageString = "onMessage=\"" + message
                + "\" customContentString=" + customContentString;
        Log.d(TAG, messageString);
        Intent intent = new Intent(ACTION_REMOTE_MESSAGE);
        intent.putExtra(MESSAGE_TAG, message);
        intent.putExtra(CUSTOM_MESSAGE_TAG, customContentString);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }
    @Override
    public void onNotificationArrived(Context context, String title,
                                      String description, String customContentString) {

        String notifyString = "onNotificationArrived  title=\"" + title
                + "\" description=\"" + description + "\" customContent="
                + customContentString;
        Log.d(TAG, notifyString);
        Intent intent = new Intent(ACTION_REMOTE_MESSAGE);
        intent.putExtra(MESSAGE_TITLE_TAG, title);
        intent.putExtra(MESSAGE_TAG, description);
        intent.putExtra(CUSTOM_MESSAGE_TAG, customContentString);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onNotificationClicked(Context context, String title,
                                      String description, String customContentString) {
        String notifyString = "onNotificationClicked title=\"" + title + "\" description=\""
                + description + "\" customContent=" + customContentString;
        Log.d(TAG, notifyString);
        Intent intent = new Intent(ACTION_NOTIFICATION_CLICKED);
        intent.putExtra(MESSAGE_TITLE_TAG, title);
        intent.putExtra(MESSAGE_TAG, description);
        intent.putExtra(CUSTOM_MESSAGE_TAG, customContentString);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

    }

    @Override
    public void onSetTags(Context context, int errorCode,
                          List<String> successTags, List<String> failTags, String requestId) {
        String responseString = "onSetTags errorCode=" + errorCode
                + " successTags=" + successTags + " failTags=" + failTags
                + " requestId=" + requestId;
        Log.d(TAG, responseString);
    }

    @Override
    public void onDelTags(Context context, int errorCode,
                          List<String> successTags, List<String> failTags, String requestId) {
        String responseString = "onDelTags errorCode=" + errorCode
                + " successTags=" + successTags + " failTags=" + failTags
                + " requestId=" + requestId;
        Log.d(TAG, responseString);
    }
    @Override
    public void onListTags(Context context, int errorCode, List<String> tags,
                           String requestId) {
        String responseString = "onListTags errorCode=" + errorCode + " tags="
                + tags;
        Log.d(TAG, responseString);
    }
    @Override
    public void onUnbind(Context context, int errorCode, String requestId) {
        String responseString = "onUnbind errorCode=" + errorCode
                + " requestId = " + requestId;
        Log.d(TAG, responseString);
        Intent intent = new Intent(ACTION_REMOTE_MESSAGE_UNBIND);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

}
