package co.igloohome.baidu_push

import io.flutter.plugin.common.PluginRegistry.Registrar


class BaiduPushPlugin{
  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      BaiduPushMessagePlugin(registrar.messenger(), registrar.context())
    }
  }
}
