package co.igloohome.baidu_push

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter

import androidx.localbroadcastmanager.content.LocalBroadcastManager
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import com.baidu.android.pushservice.PushConstants
import com.baidu.android.pushservice.PushManager
import io.flutter.plugin.common.BinaryMessenger

class BaiduPushMessagePlugin(binaryMessenger: BinaryMessenger, private var context: Context) : BroadcastReceiver(), MethodCallHandler {
    companion object {
        private const val ACTION_REMOTE_MESSAGE = "baidu.push.message"
        private const val ACTION_REMOTE_MESSAGE_BIND = "baidu.push.bind"
        private const val ACTION_REMOTE_MESSAGE_UNBIND = "baidu.push.unbind"
        private const val ACTION_NOTIFICATION_CLICKED = "baidu.push.notif.clicked"
        private const val MESSAGE_TITLE_TAG="message_title"
        private const val MESSAGE_TAG = "message_description"
        private const val CUSTOM_MESSAGE_TAG = "message_content"
        private const val TOKEN_TAG = "token"

    }
    private var token=""
    private var channel: MethodChannel = MethodChannel(binaryMessenger, "baidu_push")

    init {
        val intentFilter = IntentFilter()
        intentFilter.addAction(ACTION_REMOTE_MESSAGE_BIND)
        intentFilter.addAction(ACTION_REMOTE_MESSAGE)
        intentFilter.addAction(ACTION_REMOTE_MESSAGE_UNBIND)
        intentFilter.addAction(ACTION_NOTIFICATION_CLICKED)
        val manager = LocalBroadcastManager.getInstance(context)
        manager.registerReceiver(this, intentFilter)
        channel.setMethodCallHandler(this)
    }


    override fun onMethodCall(call: MethodCall, result: Result) {
        when {
            "configure" == call.method ->
          
                PushManager.startWork(context, PushConstants.LOGIN_TYPE_API_KEY,
                        call.argument("baidu_key"))

            "getToken" == call.method -> {
                result.success(token)
            }
            else -> result.notImplemented()
        }
    }


    override fun onReceive(context: Context?, intent: Intent?) {
        when (intent?.action ?: return) {
            ACTION_REMOTE_MESSAGE_BIND -> {
                val token = intent.getStringExtra(TOKEN_TAG)
                this.token=token
                channel.invokeMethod("onToken", token)
            }
            ACTION_REMOTE_MESSAGE -> {
                val messageData=mapOf(
                        "title" to intent.getStringExtra(MESSAGE_TITLE_TAG),
                        "description" to intent.getStringExtra(MESSAGE_TAG),
                        "data" to intent.getStringExtra(CUSTOM_MESSAGE_TAG))
                channel.invokeMethod("onMessage", messageData)
            }
            ACTION_NOTIFICATION_CLICKED -> {
                val messageData=mapOf(
                        "description" to intent.getStringExtra(MESSAGE_TAG),
                        "data" to intent.getStringExtra(CUSTOM_MESSAGE_TAG))
                channel.invokeMethod("onLaunch", messageData)
            }
        }

    }
}

