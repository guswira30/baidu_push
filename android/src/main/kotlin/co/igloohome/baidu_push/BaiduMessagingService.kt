package co.igloohome.baidu_push


import android.content.Context
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.baidu.android.pushservice.PushMessageReceiver

import android.content.Intent
import android.util.Log


/*
 * 0 - Success
 * 10001 - Network Problem
 * 10101 - Integrate Check Error
 * 30600 - Internal Server Error
 * 30601 - Method Not Allowed
 * 30602 - Request Params Not Valid
 * 30603 - Authentication Failed
 * 30604 - Quota Use Up Payment Required
 * 30605 - Data Required Not Found
 * 30606 - Request Time Expires Timeout
 * 30607 - Channel Token Timeout
 * 30608 - Bind Relation Not Found
 * 30609 - Bind Number Too Many
 */

class BaiduMessagingService : PushMessageReceiver() {
    val ACTION_REMOTE_MESSAGE = "baidu.push.message"
    val ACTION_REMOTE_MESSAGE_BIND = "baidu.push.bind"
    val ACTION_REMOTE_MESSAGE_UNBIND = "baidu.push.bind"
    private val MESSAGE_TAG = "message"
    private val CUSTOM_MESSAGE_TAG = "message"
    private val TOKEN_TAG = "token"

    override fun onBind(context: Context, errorCode: Int, appid: String,
                        userId: String, channelId: String, requestId: String) {
        print("on bind called")
        Log.d(TAG, "onbind call")
        val responseString = ("onBind errorCode=" + errorCode + " appid="
                + appid + " userId=" + userId + " channelId=" + channelId
                + " requestId=" + requestId)
        Log.d(TAG, responseString)
        val intent = Intent(ACTION_REMOTE_MESSAGE_BIND)
        intent.putExtra(TOKEN_TAG, userId)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    //on message receive
    override fun onMessage(context: Context, message: String,
                           customContentString: String) {
        val intent = Intent(ACTION_REMOTE_MESSAGE)
        intent.putExtra(MESSAGE_TAG, message)
        intent.putExtra(CUSTOM_MESSAGE_TAG, customContentString)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

    override fun onNotificationArrived(context: Context, title: String,
                                       description: String, customContentString: String) {

        val notifyString = ("通知到达 onNotificationArrived  title=\"" + title
                + "\" description=\"" + description + "\" customContent="
                + customContentString)
        Log.d(TAG, notifyString)
    }

    override fun onSetTags(context: Context, errorCode: Int,
                           successTags: List<String>, failTags: List<String>, requestId: String) {
        val responseString = ("onSetTags errorCode=" + errorCode
                + " successTags=" + successTags + " failTags=" + failTags
                + " requestId=" + requestId)
        Log.d(TAG, responseString)
    }

    override fun onDelTags(context: Context, errorCode: Int,
                           successTags: List<String>, failTags: List<String>, requestId: String) {
        val responseString = ("onDelTags errorCode=" + errorCode
                + " successTags=" + successTags + " failTags=" + failTags
                + " requestId=" + requestId)
        Log.d(TAG, responseString)
    }


    override fun onListTags(context: Context, errorCode: Int, tags: List<String>,
                            requestId: String) {
        val responseString = ("onListTags errorCode=" + errorCode + " tags="
                + tags)
        Log.d(TAG, responseString)
    }

    override fun onUnbind(context: Context, errorCode: Int, requestId: String) {
        val responseString = ("onUnbind errorCode=" + errorCode
                + " requestId = " + requestId)
        Log.d(TAG, responseString)
        val intent = Intent(ACTION_REMOTE_MESSAGE_UNBIND)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)

    }

    override fun onNotificationClicked(p0: Context?, p1: String?, p2: String?, p3: String?) {

    }

    companion object {
        val TAG: String = BaiduMessagingService::class.java
                .simpleName
    }

}
