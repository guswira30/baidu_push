#import "BaiduPushPlugin.h"
#import <baidu_push/baidu_push-Swift.h>

@implementation BaiduPushPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBaiduPushPlugin registerWithRegistrar:registrar];
}
@end
