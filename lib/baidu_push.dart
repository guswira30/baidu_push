import 'dart:async';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';
import 'package:platform/platform.dart';



typedef Future<dynamic> MessageHandler(Map<String, dynamic> message);


class BaiduPush {
  factory BaiduPush() => _instance;

  @visibleForTesting
  BaiduPush.private(MethodChannel channel, Platform platform)
      : _channel = channel,
        _platform = platform;

  static final BaiduPush _instance = BaiduPush.private(
      const MethodChannel('baidu_push'),
      const LocalPlatform());

  final MethodChannel _channel;
  final Platform _platform;
  MessageHandler _onToken;
  MessageHandler _onMessage;
  MessageHandler _onLaunch;
  MessageHandler _onResume;



  /// Sets up [MessageHandler] for incoming messages.
  void configure({
    MessageHandler onMessage,
    MessageHandler onToken,
    MessageHandler onBackgroundMessage,
    MessageHandler onLaunch,
    MessageHandler onResume,
    String baiduKey
  }) {
    _onMessage = onMessage;
    _onLaunch = onLaunch;
    _onResume = onResume;
    _channel.setMethodCallHandler(_handleMethod);
    _channel.invokeMethod<void>('configure', {"baidu_key":baiduKey});
  }

  final StreamController<String> _tokenStreamController =
  StreamController<String>.broadcast();

  /// Fires when a new token is generated.
  Stream<String> get onTokenRefresh {
    return _tokenStreamController.stream;
  }

  /// Returns the token.
  Future<String> getToken() async {
    return await _channel.invokeMethod<String>('getToken');
  }

  Future<dynamic> _handleMethod(MethodCall call) async {
    print("called method handler in dart");
    print(call.method);
    print(call.arguments);
    switch (call.method) {
      case "onToken":
        final String token = call.arguments;
        _tokenStreamController.add(token);
        return null;
      case "onMessage":
        return _onMessage(call.arguments.cast<String, dynamic>());
      case "onLaunch":
        return _onLaunch(call.arguments.cast<String, dynamic>());
      case "onResume":
        return _onResume(call.arguments.cast<String, dynamic>());
      default:
        throw UnsupportedError("Unrecognized JSON message");
    }
  }
}

